import localData from '../static/data.json';

export const state = () => ({
    courses: []
})

export const mutations = {
    setCourses(state, courses) {
        state.courses = courses
    }
}

export const actions = {
    async fetchCourses({commit}) {
        const courses = await localData;
        commit('setCourses', courses)
    }
}

export const getters = {
    courses: c => c.courses,
    courseById: (state) => (id) => {
        return id ? state.courses.find(item => item.id == id) : {}
    }  
}